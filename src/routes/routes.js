import { Router } from 'express';
import UserController from '../controllers/users.controller';
import SubjectController from '../controllers/subjects.controller';
import SubjectCategoryController from '../controllers/subjectCategory.controller';
import ArticleController from '../controllers/article.controller';

const router = Router();

router.get('/', function(req, res){
    res.send("test, I'm working");
});

//Users
router.post('/users', UserController.create);
router.get('/users', UserController.list);
router.get('/users/:id', UserController.details);
router.delete('/users/:id', UserController.delete);
router.put('/users/:id', UserController.update);
router.post('/users/authenticate', UserController.authenticate);

//Subject
router.post('/subjects', SubjectController.create);
router.delete('/subjects/:id', SubjectController.delete);
router.put('/subjects/:id', SubjectController.update);
router.get('/subjects/:id', SubjectController.details);
router.get('/subjects', SubjectController.list);
router.post('/subjects/search', SubjectController.search);

//Comments
router.get('/subjects/comments/:id', SubjectController.listSubjectComments);
router.post('/subjects/:id/comments', SubjectController.addComment);
router.delete('/subjects/:subjectId/:commentId', SubjectController.deleteComment);

//SubjectCategory
router.post('/subjectCategories', SubjectCategoryController.create);
router.get('/subjectCategories', SubjectCategoryController.list);
router.put('/subjectCategories/:id', SubjectCategoryController.update);
router.delete('/subjectCategories/:id', SubjectCategoryController.delete);

//Articles
router.post('/articles', ArticleController.create);
router.put('/articles/:id', ArticleController.update);
router.get('/articles', ArticleController.list);
router.get('/articles/:id', ArticleController.details);
router.delete('/articles/:id', ArticleController.delete);


export default router;