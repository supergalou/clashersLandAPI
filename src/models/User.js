import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        require: true
    },
    pseudo: {
        type: String,
        unique: true 
    },
    email: {
        type: String,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    user_role:{
        type: Number,
        default: 1
    }
});

const User = mongoose.model('User', userSchema);
export default User;