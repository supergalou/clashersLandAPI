import User from "../models/User";
import jwt from 'jsonwebtoken';

export default class UserController {
    /**
     * Creates a User in a database
     * @param {Request} req 
     * @param {Response} res 
     */

    static async create(req, res){
        let status = 200;
        let body = {};

        try{
            console.log("req.body : " + JSON.stringify(req.body));
            let newUser = await User.create({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                pseudo: req.body.pseudo,
                email: req.body.email,
                password: req.body.password
            });

            body = {
                newUser,
                'message': 'User created'
            }

            
            
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }

        console.log('I returned ' + JSON.stringify(body));
        return res.status(status).json(body);
    }

    static async list(req, res) {
        let status = 200;
        let body = {};

        try{
            let users = await User.find().select('-password -__v');

            body = {users, 'message': 'Users list'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        

        return res.status(status).json(body);
    }

    static async details(req, res) {
        let status = 200;
        let body = {};

        try{
            let id = req.params.id;
            console.log(id);
            let user = await User.findById(id);

            body = {user, 'message': 'User asked'}

        }catch(error){
            status = 500;
            body = {'message':error.message}
        }

        return res.status(status).json(body);
    }

    static async delete(req, res) {
        let status = 200;
        let body = {};

        try{
            console.log(req.params.id);
            await User.remove({_id: req.params.id});

            body = {'message': 'Users deleted'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        

        return res.status(status).json(body);
    }

    static async update(req, res) {
        let status = 200;
        let body = {};

        console.log(JSON.stringify(req.body));

        try{
            let user = await User.findByIdAndUpdate({_id: req.params.id},
                {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    pseudo: req.body.pseudo,
                    user_role: req.body.user_role,
                    email: req.body.email
                },
                {new: true});
            console.log(req.params.id);

            body = {user, 'message': 'Users updated'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }

        return res.status(status).json(body);
    }

    static async authenticate(req, res){
        let status = 200;
        let body = {};
        
        try {
            console.log(req.body);
            let user = await User.findOne({email: req.body.email});
            if(user && user.password === req.body.password){
                let token = jwt.sign({sub: user._id}, process.env.AUTHKEY);
                body = {
                    user,
                    token,
                    'message' : 'Authenticated User'
                }
            }else{
                status = 401;
                if(user){
                    body = {'message': 'Password Error'}
                }else{
                    body = {'message': 'Email Error'}
                }
            }
        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }

        return res.status(status).json(body);
    } 
}