import Subject from "../models/Subject";
import SubjectCategory from "../models/SubjectCategory";
import User from "../models/User";

export default class SubjectController {

    /**
    * Creates a Subject in a database
    * @param {Request} req 
    * @param {Response} res 
    */

    static async create(req, res){
       let status = 200;
       let body = {};

       try{
           let newSubject = await Subject.create({
               userId: req.body.userId,
               categoryId: req.body.categoryId,
               date: Date.now(),
               title: req.body.title,
               content: req.body.content
           });

            body = {
                newSubject,
                'message': 'Subject created'
            }

        }catch(error){
            status = 500;
            body = {'message':error.message}
        }

        return res.status(status).json(body);
    }

    static async update(req, res) {
        let status = 200;
        let body = {};

        try{
            let subject = await Subject.findOneAndUpdate({_id: req.params.id},
                {
                    userId: req.body.userId,
                    categoryId: req.body.categoryId,
                    title: req.body.title,
                    content: req.body.content,
                },
                {new: true});
                
            console.log(req.params.id);       
            body = {subject, 'message': 'Subject updated'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        

        return res.status(status).json(body);
    }

    static async delete(req, res) {
        let status = 200;
        let body = {};

        try{
            console.log(req.params.id);
            await Subject.remove({_id: req.params.id});

            body = {'message': 'Subject deleted'}
        }catch(error){
            status = 500;
            body = {'message':error.message}
        }
        

        return res.status(status).json(body);
    }

    static async list (req, res) {
        let status = 200;
        let body = {}; 
        console.log("asking for subjects")
        try {
            let subjects = await Subject.find().select('-__v');
            let subjecsWithCatAndUser = [];

            for(const item of subjects){
                let cat = await SubjectCategory.findById(item.categoryId);
                let user = await User.findById(item.userId);

                console.log(JSON.stringify(item));
                console.log(JSON.stringify(user));
                
                subjecsWithCatAndUser.push({
                    _id: item._id, 
                    userId: item.userId,
                    categoryId: item.categoryId,
                    date: item.date,
                    title: item.title,
                    content: item.content,
                    userPseudo: user.pseudo,
                    catName: cat.name
                })
            }
            // console.log(JSON.stringify(subjecsWithCatAndUser));
            body = { "subjects": subjecsWithCatAndUser, 'message': 'Subjects list'}
        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async search (req, res) {
        let status = 200;
        let body = {}; 
        console.log("searching for subjects")
        try {

            let subjects = await Subject.find().select('-__v');
            let subjecsWithCatAndUser = [];
            console.log(req.body);
            if(req.body.categoryId !== undefined && req.body.search !== undefined){
                for(const item of subjects){
                    console.log(item.categoryId);
                    if((req.body.categoryId === "" || req.body.categoryId == item.categoryId) && item.content.toLowerCase().includes(req.body.search.toLowerCase())){
                        let cat = await SubjectCategory.findById(item.categoryId);
                        let user = await User.findById(item.userId);
        
                        //console.log(JSON.stringify(item));
                        //console.log(JSON.stringify(user));
                        
                        subjecsWithCatAndUser.push({
                            _id: item._id, 
                            userId: item.userId,
                            categoryId: item.categoryId,
                            date: item.date,
                            title: item.title,
                            content: item.content,
                            userPseudo: user.pseudo,
                            catName: cat.name
                        })
                    }
                    
                }
            }else{
                subjecsWithCatAndUser = subjects;
            }

            
            // console.log(JSON.stringify(subjecsWithCatAndUser));
            body = { "subjects": subjecsWithCatAndUser, 'message': 'Subjects list'}
        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let subject = await Subject.findById({_id: req.params.id});

            let cat = await SubjectCategory.findById(subject.categoryId);
            let user = await User.findById(subject.userId);


            let comments =  subject.comments;

            let commentsFull = [];

            for(const item of comments){
                let commentUser = await User.findById(item.userId);

                commentsFull.push({
                    _id: item._id,
                    userId: item.userId,
                    date: item.date,
                    comment: item.comment,
                    userPseudo: commentUser.pseudo
                });
            }

            body = {
                _id: subject._id, 
                userId: subject.userId,
                categoryId: subject.categoryId,
                date: subject.date,
                title: subject.title,
                content: subject.content,
                userPseudo: user.pseudo,
                catName: cat.name, 
                comments: commentsFull
            }

        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    
    static async listSubjectComments(req, res){
        let status = 200;
        let body = {};

        try {
            let subject = await Subject.findById({_id: req.params.id}).select('comments');
            console.log(subject.comments);
            body = subject.comments
        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async addComment(req, res) {
        let status = 200;
        let body = {};

        try {

            let subject = await Subject.findById({_id: req.params.id}).select('comments');
            
            let myComments = subject.comments
            let newComment = {
                userId: req.body.userId,
                date: new Date,
                comment: req.body.comment
            }
            myComments.push(newComment);

            await subject.update({
                comments: myComments
            });

            body = {'body': subject.comments, 'message': 'comment added' };
        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }

    static async deleteComment (req, res) {
        let status = 200;
        let body = {};

        try {
            let subject = await Subject.findById({_id: req.params.subjectId}).select('comments');
            let comments = subject.comments;
             console.log(comments);
            comments.splice(comments.findIndex(com => com._id === req.params.commentId),1);
            await subject.update({
                comments: comments
            });

            body = {'comments': subject.comments, 'message': 'Comment deleted'}
        } catch (error) {
            status = 500;
            body = {'message':error.message}
        }
        return res.status(status).json(body);
    }
}